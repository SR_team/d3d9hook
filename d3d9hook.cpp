#include "d3d9hook.h"

d3d9hook::d3d9hook( IDirect3DDevice9 *pOriginal ) {
	hook = new SRHookVtableExt<IDirect3DDevice9>( pOriginal );
	hook->hookComMethod( &IDirect3DDevice9::Reset, this, &d3d9hook::Reset );
	hook->hookComMethod( &IDirect3DDevice9::Present, this, &d3d9hook::Present );
	hook->hookComMethod( &IDirect3DDevice9::BeginScene, this, &d3d9hook::BeginScene );
	hook->hookComMethod( &IDirect3DDevice9::EndScene, this, &d3d9hook::EndScene );
	hook->hookComMethod( &IDirect3DDevice9::Clear, this, &d3d9hook::Clear );
}

d3d9hook::~d3d9hook( void ) {
	hook->restoreAll();
}

HRESULT d3d9hook::Reset( IDirect3DDevice9 *self, D3DPRESENT_PARAMETERS *pPresentationParameters ) {
	onPreReset();
	onHookReset( pPresentationParameters );
	auto result = hook->callOriginalComMethod( &IDirect3DDevice9::Reset, self, pPresentationParameters );
	if ( result == D3D_OK )
		onPostReset();
	else
		onPostResetFail();
	return result;
}

HRESULT d3d9hook::Present( IDirect3DDevice9 *self, CONST RECT *pSourceRect, CONST RECT *pDestRect,
									   HWND hDestWindowOverride, CONST RGNDATA *pDirtyRegion ) {
	onDraw();
	onHookPresent( pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion );
	return hook->callOriginalComMethod( &IDirect3DDevice9::Present, self, pSourceRect, pDestRect,
										hDestWindowOverride, pDirtyRegion );
}

HRESULT d3d9hook::BeginScene( IDirect3DDevice9 *self ) {
	auto result = hook->callOriginalComMethod( &IDirect3DDevice9::BeginScene, self );
	onBeginScene();
	return result;
}

HRESULT d3d9hook::EndScene( IDirect3DDevice9 *self ) {
	onEndScene();
	return hook->callOriginalComMethod( &IDirect3DDevice9::EndScene, self );
}

HRESULT d3d9hook::Clear( IDirect3DDevice9 *self, DWORD rect_count, const D3DRECT *rects, DWORD flags,
						 D3DCOLOR color, float z, DWORD stencil ) {
	onHookClear( rect_count, rects, flags, color, z, stencil );
	auto result = hook->callOriginalComMethod( &IDirect3DDevice9::Clear, self, rect_count, rects, flags,
											   color, z, stencil );
	onClear();
	return result;
}

IDirect3DDevice9 *d3d9hook::device() {
	return hook->getObject();
}
