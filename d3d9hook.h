#ifndef D3D9HOOK_H
#define D3D9HOOK_H

#include <d3d9.h>
#include <d3dx9.h>
#include <d3dx9core.h>
#include <wingdi.h>
#include <SRHookVtableExt.hpp>
#include <SRSignal.hpp>

/**
 * @brief Класс хуков методов COM-объекта IDirect3DDevice9
 * @details При вызове деструктора восстанавливаются оригинальные методы, но объект SRHookVtableExt не
 * удаляется, для обеспечения прослойки совместимости между хукаемым девайсом и потенциально возможными хуками
 * выше установленного этим классом хука
 */
class d3d9hook {
public:
	/**
	 * @brief Конструктор класса
	 * @param pOriginal девайс, который будет хукнут
	 */
	d3d9hook( IDirect3DDevice9 *pOriginal );
	/**
	 * @brief Виртуальный деструктор
	 */
	virtual ~d3d9hook( void );

protected: // Хуки
	HRESULT Reset( IDirect3DDevice9 *self, D3DPRESENT_PARAMETERS *pPresentationParameters );
	HRESULT Present( IDirect3DDevice9 *self, CONST RECT *pSourceRect, CONST RECT *pDestRect,
					 HWND hDestWindowOverride, CONST RGNDATA *pDirtyRegion );
	HRESULT BeginScene( IDirect3DDevice9 *self );
	HRESULT EndScene( IDirect3DDevice9 *self );
	HRESULT Clear( IDirect3DDevice9 *self, DWORD rect_count, const D3DRECT *rects, DWORD flags,
				   D3DCOLOR color, float z, DWORD stencil );

public:
	/// Вызывается до вызова Reset
	SRSignal<> onPreReset;
	/// Вызывается после вызова Reset, если он вернул D3D_OK
	SRSignal<> onPostReset;
	/// Вызывается после вызова Reset, если он вернул \b не D3D_OK
	SRSignal<> onPostResetFail;
	/// Вызывается перед вызовом Present
	SRSignal<> onDraw;
	/// Вызывается после вызова BeginScene
	SRSignal<> onBeginScene;
	/// Вызывается перед вызовом EndScene
	SRSignal<> onEndScene;
	/// Вызывается после вызова Clear
	SRSignal<> onClear;

	/// Вызывается перед вызовом Reset и позволяет изменить передаваемые в Reset параметры
	SRSignal<D3DPRESENT_PARAMETERS *&> onHookReset;
	/// Вызывается перед вызовом Present и позволяет изменить передаваемые в Present параметры
	SRSignal<CONST RECT *&, CONST RECT *&, HWND &, CONST RGNDATA *&> onHookPresent;
	/// Вызывается перед вызовом Clear и позволяет изменить передаваемые в Clear параметры
	SRSignal<DWORD &, const D3DRECT *&, DWORD &, D3DCOLOR &, float &, DWORD &> onHookClear;

	/// Возвращает переданный в конструктор девайс
	IDirect3DDevice9 *device();

protected:
	/// хук vtable переданного девайса
	SRHookVtableExt<IDirect3DDevice9> *hook;
};

#endif // D3D9HOOK_H
